from django.urls import path
from . import views
urlpatterns = [
    path('', views.index, name="index"),
    path('news/', views.news, name="news"),
    path('search/', views.search, name="search"),
    path('categories/', views.categories, name='categories'),
    path('news/<slug:slug>',views.post_detail,name="post_detail_url"),
    path('news/<slug:slug>/leave_comment/', views.comment, name='comment'),
    path('categories/<slug:slug>/', views.category_detail, name="category_detail_url"),
    path('register/', views.register, name='register'),
    path('contact/', views.contact, name='contact'),
    path('popular/', views.popular, name="popular"),
]